Metadata-Version: 2.1
Name: mockaioredis
Version: 0.0.16
Summary: Mock implementation of aioredis
Home-page: https://github.com/kblin/mockaioredis
Author: Kai Blin
Author-email: kblin@biosustain.dtu.dk
License: Apache Software License
Platform: POSIX
Classifier: License :: OSI Approved :: Apache Software License
Classifier: Development Status :: 3 - Alpha
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.5
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Operating System :: POSIX
Classifier: Environment :: Web Environment
Classifier: Intended Audience :: Developers
Classifier: Topic :: Software Development
Classifier: Topic :: Software Development :: Libraries
Description-Content-Type: text/markdown
Provides-Extra: testing
Provides-Extra: shipping
License-File: LICENSE

Mock library to replace aioredis during unit tests
==================================================

[![Build Status](https://travis-ci.com/kblin/mockaioredis.svg?branch=master)](https://travis-ci.com/kblin/mockaioredis)

mockaioredis is to [aioredis] what the [mockredispy] library is to plain [redis-py].
It uses the mockredispy library and wraps it in the asyncio magic required to work like
aioredis.

Uses the new `async` keyword for Python 3.5, so no 3.4 support.

**Beware**: This is an early alpha that isn't even close to API-complete.
In fact, so far it only supports the limited set of calls I needed for another project.
Eventually, as I use more and more aioredis calls in my other projects, this mock layer
will be fleshed out more.


Installation
------------

You can install `mockaioredis` from PYPI by running `pip install mockaioredis`.

If you want to update an existing install, run `pip install --update mockaioredis`.

You can also clone this repository from github and run `pip install .` from the repository base directory.


Usage
-----

You can use it as a fixture using pytest-mock

```python
import mockaioredis

@pytest.fixture(autouse=True)
def redis(mocker):
    """Mock Redis."""
    mocker.patch.object(aioredis, 'create_pool', new=mockaioredis.create_pool)
```


License
-------
Like mockredispy, mockaioredis is licensed under the Apache License, Version 2.0.
See [`LICENSE`](LICENSE) for details.

[aioredis]: https://github.com/aio-libs/aioredis
[mockredispy]: https://github.com/locationlabs/mockredis
[redis-py]: https://github.com/andymccurdy/redis-py
